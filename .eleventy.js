module.exports = function (eleventyConfig) {
  eleventyConfig.setTemplateFormats(["html", "css", "png", "svg", "txt", "js"]);
};
